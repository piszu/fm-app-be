package pl.mariuszpisz.fmapp.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.mariuszpisz.fmapp.common.exceptions.*;
import pl.mariuszpisz.fmapp.common.models.ExceptionResponse;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
            ResourceAlreadyExistException.class,
            InvalidInputDataException.class
    })
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex) {

        return buildResponseEntity(new ExceptionResponse(BAD_REQUEST, ex.getMessage()));
    }

    @ExceptionHandler(value = {
            ResourceNotFoundException.class,
            UserNotFoundException.class,
            UserAuthorizationNotFoundException.class
    })
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex) {

        return buildResponseEntity(new ExceptionResponse(NOT_FOUND, ex.getMessage()));
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleOtherUnspecifiedExceptions(
            RuntimeException ex, WebRequest request) {

        return buildResponseEntity(new ExceptionResponse(
                INTERNAL_SERVER_ERROR, ex.getMessage()));
    }

    private ResponseEntity<Object> buildResponseEntity(ExceptionResponse exceptionResponse) {
        return new ResponseEntity<>(exceptionResponse, exceptionResponse.getStatus());
    }
}