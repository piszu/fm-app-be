package pl.mariuszpisz.fmapp.common.exceptions;

import lombok.Getter;

@Getter
public class UnimplementedException extends RuntimeException {
    public UnimplementedException() {
    }

    public UnimplementedException(String message) {
        super(message);
    }
}
