package pl.mariuszpisz.fmapp.common.exceptions;

public class InactiveAuthorizationException extends RuntimeException {
    public InactiveAuthorizationException() {
    }

    public InactiveAuthorizationException(String message) {
        super(message);
    }
}