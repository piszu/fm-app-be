package pl.mariuszpisz.fmapp.common.exceptions;

public class UserNotFoundException extends ResourceNotFoundException {
    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
